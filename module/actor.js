/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
import { DSARoll } from "./dice.js";
export class Dsa41Actor extends Actor {

  /** @override */
  getRollData() {
    const data = super.getRollData();
    const shorthand = game.settings.get("dsa41", "macroShorthand");

    // // Re-map all attributes onto the base roll data
    // if ( !!shorthand ) {
    //   for ( let [k, v] of Object.entries(data.attributes) ) {
    //     if ( !(k in data) ) data[k] = v.value;
    //   }
    //   delete data.attributes;
    // }

    // Map all items data using their slugified names
    // data.items = this.data.items.reduce((obj, i) => {
    //   let key = i.name.slugify({strict: true});
    //   let itemData = duplicate(i.data);
    //   if ( !!shorthand ) {
    //     for ( let [k, v] of Object.entries(itemData.attributes) ) {
    //       if ( !(k in itemData) ) itemData[k] = v.value;
    //     }
    //     delete itemData["attributes"];
    //   }
    //   obj[key] = itemData;
    //   return obj;
    // }, {});
    return data;
  }

  rollAttribute(abilityId, options = { event: null }) {
    const label = abilityId
    let actorData = this.data;
    const abl = actorData.data.attributes[abilityId];
    let exp = `1d20`;
    //Check and add Modifiers
    const rollParts = [exp];
    let opt = {};
    opt.target = actorData.data.attributes[abilityId].current;
    opt.rollUnder = true;
    // Roll and return
    return DSARoll({
        event: options.event,
        parts: rollParts,
        data: actorData,
        speaker: ChatMessage.getSpeaker({ actor: this }),
        flavor: label,
        title: label,
        options: opt
    });
  } 

  rollCombatSkill(skillId, options = { event: null }) {
    const label = skillId
    let actorData = this.data;
    let exp = `1d20`;
    //Check and add Modifiers
    const rollParts = [exp];
    let opt = {};
    opt.target = actorData.data.combat[skillId];
    opt.rollUnder = true;
    // Roll and return
    return DSARoll({
        event: options.event,
        parts: rollParts,
        data: actorData,
        speaker: ChatMessage.getSpeaker({ actor: this }),
        flavor: label,
        title: label,
        options: opt
    });
  } 

  rollTalent(talentId, options = { event: null }) {
    let items = this.items.filter((i) => i.id == talentId);
    if (!items.length) {
        return;
    }
    let talent = items[0].data['data'];
    const label = items[0].name;
    let actorData = this.data;
    let talent_value = talent.value;
    if(!talent_value) {
      talent_value = 0;
    }
    let exp = ''
    for(let i=0; i < 3; i++) {
      let attribute_name = talent.attributes[i].trim();
      let attribute = actorData.data.attributes[attribute_name].current;
      exp = exp + '{' + attribute + '-1d20+({' + talent_value +',0}kl),0}kl +';
    }
    exp = exp + '{' + talent_value + ',0}kh';
    //Check and add Modifiers
    const rollParts = [exp];
    let opt = {};
    opt.target = 0;
    opt.rollUnder = false;
    // Roll and return
    return DSARoll({
        event: options.event,
        parts: rollParts,
        data: actorData,
        speaker: ChatMessage.getSpeaker({ actor: this }),
        flavor: label,
        title: label,
        options: opt
    });
  } 

  lifeEnergy() {
    let attributes = this.data.data.attributes;
    return (2*attributes.KO+attributes.KK)/2;
  }
}
