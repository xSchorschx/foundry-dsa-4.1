This repository is abandoned. I have joined another project which is developing a Foundry DSA4.1 module. The new project will be linked here, as soon as it is public.

# DSA4.1 (The Dark Eye) System for Foundry VTT

A Foundry VTT Game System for DSA4.1 in a very early stage.

The HTML for the templates is currently a copy of the Roll20 character sheet of DSA 4.1. Therefore you will see many input fields which are currently not working. That what currently is working is:

* Main attributes: Editing values and making a roll based on the values
* Stats (health, endurance,...): Editing values
* Talents: Editing values and making a 3d20 roll based on its value and the associated attributes

    * Talents are saved in a compendium pack and currently not provided in this repository (not sure about the legal situation)
	* In the scripts directory is a python script which can extract the talents from the "Wege des Schwertes" PDF file (actually you need the 4 pages with the talents table)
	* The extracted talents can be imported with the importTalents function in dsa41.js